Storing Sensor Data
===================

eGenie exposes a URL to which sensors can send data, but you need to do a little configuration first. 

From the eGenie admin panel (see the Configuration), head to the SD_STORE section and click on Sensors. Two are already created (Office Electricity and Office Temperature). Click on Office Temperature, and you will see that it is a temperature sensor, using the temperature channel. 

At the bottom is a `RAWDATAKEY` section. Click on the `+` to the right of the dropdown, and enter a random string of characters in the popup (we will use `ab124ed` in this example). Then press `SAVE` here, and again on the Sensor page. 

You are now able to send sensor readings to this sensor! If you are running locally, you should be able to do the following:

```
curl -X POST -F 'key=ab124ed' -F 'value=50' -F 'dt=2017-12-04T15:21:10' \
 "http://127.0.0.1:8000/sd_store/rawinput/sensor/temperature_1/temperature/data/"
```
This adds a value of 50, at the date and time specified. If you don't specify a `dt` value, the current server date and time are used.

Important: The URL is _insecure_, which is why the raw data key is provided to allow some level of authentication. You should keep this key strictly on the device sending data to the server, and on the server itself.