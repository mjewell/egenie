Configuring eGenie
==================

Initial Configuration
---------------------

<h3>Environmental Variables</h3>

The eGenie docker-compose files expect to see an env in the same directory, which gives the MySQL credentials as below:

```
MYSQL_DATABASE=egenie
MYSQL_USER=egenie
MYSQL_PASSWORD=eg3n13
MYSQL_ROOT_PASSWORD=2390580329480
```

As well as being used to set up the MySQL server, these are also used by Django to configure the connection to the eGenie database. 

<h3>Django Settings</h3>

Inside `src/egenie/egenie/settings` you will find three python files:

* `base.py`: contains the vast majority of eGenie's settings, from installed applications to template folders. This is overridden by the following files.
* `local.py`: settings for running eGenie on a local machine. You'll see that this is the default used in `manage.py`. Uses a SQLite database, has `DEBUG` set to True for helpful error messages, and uses the console email backend to display emails that would be sent.
* `production.py`: settings for running eGenie under Docker. This pulls in the MySQL credentials mentioned earlier, keeps `DEBUG` set to False, and sets the `ALLOWED_HOSTS` variable to allow everyone to reach the site.

<h4>Offsite / Onsite Settings</h4>

eGenie operates in two modes - offsite, and onsite. It uses some simple IP address checking to see if the visitor is in the building where the deployment is running, and choose the screens that should be shown based on that.

In production.py, the onsite IP addresses can be customized using the SITE_IPS setting. A prefix can be provided to match multiple addresses:

```SITE_IPS = ['62.255', '164.30.12.1']```

The screens which are displayed can then be customized like so:

```
ONSITE_SCREEN_ORDER = ['alwayson', 'temperature', 'annotation', 'pinboard:public']
OFFSITE_SCREEN_ORDER = ['alwayson', 'temperature', 'annotation']
```

In this instance, onsite visitors will see the public pinboard view, while other users will only see the always on, temperature, and annotation displays.

<h4>Contact Email</h4>

This should be set to the email address of the FM at the site, or someone responsible for seeing emails sent to the FM. It is used by the fm application to send messages from eGenie.

`FM_EMAIL = "fm@example.com"`

Site Administration
-------------------

To access the eGenie admin pages, assuming you are running it within Docker, head to `http://127.0.0.1/admin/`. You should be able to log in with the username and password you specified while installing eGenie. Some of the sections on this page will be particuluarly useful:

<h3>Deployments</h3>

Under the Deployments subheading, you can set up the details for the eGenie deployment. You should already have one at `http://127.0.0.1/admin/deployments/deployment/1/change/` which has some default settings. Note that you can specify the Sensor/Channel pairs that are part of this deployment - again, some defaults have been created for you (Office Temperature and Office Electricity). 

<h3>eGenie</h3>

This section contains eGenie-specific settings (rather than application configuration). 
* `Participants` keeps track of which users have agreed to study conditions; 
* `Plinths` describes the eGenie tablets that are deployed on-site. They have a textual `Location`, an associated `Deployment`, and an `X`,`Y` value which corresponds to the location of the plinth on the floorplan. `Printer` and `Pi ip` are only relevant if the psychology application requires pledges to be printed.
* `SensorPosition` describes the `X`,`Y` positions of each sensor on the floor plan.

<h3>SD_Store</h3>

sd_store is responsible for holding all collected sensor data.

* `Channels` describes the data types stored in sd_store. For example, one channel could be Electricity, with units of KWh and a reading frequency of 60 seconds.
* `Sensors` describes a deployed sensor, with its owner, ID number, and channels. You can also add a 'Raw Data Key' to the sensor, which is used to allow hubs to send data through to the sd_store APIs.
* `Sensor readings` contains a set of readings, with timestamps, values, and a specified sensor and channel.
