Docker Installation
===================

eGenie can be started as a set of docker containers, avoiding the setup process that would be needed otherwise.

First, install [Docker Compose](https://docs.docker.com/compose/install/) on your local machine. On Linux you will need to first install the [Docker Engine](https://docs.docker.com/engine/installation/#server).

Clone the eGenie repository, or download and extract it.

cd into the eGenie source folder, copy env.sample to env, and edit it to reflect your required database settings.

If you are on Linux, ensure you are a member of the docker user group::

    $ usermod -a -G docker username

cd into the eGenie source folder, and start up the various containers:

    $ docker-compose up -d

Once that is completed, the following commands will handle setup for you. First, we need to create an administrator - provide the email address and password you would like to use when prompted.
 
    $ docker exec -it app sh -c 'cd /app; python manage.py createsuperuser'

Finally, set up the various eGenie requirements:

    $ docker exec -it app sh -c 'cd /app; python manage.py setup_egenie'

TODO: SSL setup

TODO: Deploying to AWS / DigitalOcean?

Local Installation
==================

Head into the web app folder and set up a virtual environment:

    $ cd src/egenie
    $ python3 -m venv venv
    $ source venv/bin/activate

Install all the necessary local requirements (requirements-prod.txt is for production use).

    $ pip install -r requirements.txt

Ensure the database tables are all up to date:

    $ python manage.py migrate

Create an administrator user. Enter your email address and the password you would like to use:

    $ python manage.py createsuperuser --username admin

Create the various models that support eGenie:

    $ python manage.py setup_egenie

You can then start up your local server, which should appear at http://127.0.0.1:8000

    $ python manage.py runserver
