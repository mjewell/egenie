Customization Options
=====================

Stylesheets
-----------

* `egenie/static/egenie/css/base.css` contains the main styling for the site. 
* `[appname]/static/[appname]` contains the stylesheets for each application. These are loaded after `base.css`, so can override style choices.

Email Templates
---------------

* `egenie/templates/egenie/register_email.html` This is sent to users, along with their username, on registration. There is also a plain text equivalent, `egenie/templates/egenie/register_email.txt`.
* FM emails are sent programmatically (at present). You may wish to edit `fm/forms.py` to customize this.

Page Templates
--------------

There are a few primary templates which you may wish to change:
* `egenie/templates/egenie/base.html` This is the main template, which has several blocks that can be overridden (scripts, styles, nav, content, extra, and footercont). 
* `egenie/templates/fegenie/ront_screen.html`.This is used by all front screens - i.e. screens which will be rotated through periodically. It also has a navbar, links to the about page, and a button to launch the psychology pages.
* `egenie/templates/egenie/inner_screen.html` This allows for more significant overriding than `front_screen.html` - all it does is override base.html to ensure that the site returns to the front screen after a set duration. As it is likely the screens will be on public view, it means that it will be in a sensible default state when visited.
* `egenie/templates/egenie/info.html` Information about the deployment, that you may wish to adapt.

Floor Plans
-----------

`/static/egenie/imgs/{{tiles_folder}}/{z}/map_{x}_{y}.png`
`/static/egenie/imgs/plinth_marker.png`

Pledges
-------

TBD